/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {
  Dimensions,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ActionBar from '../components/ActionBar';
import SettingsScreenHeader from '../components/SettingsScreenHeader';
import Constant from '../utilities/Constants';
import AppMainLayout from './AppMainLayout';

const {width} = Dimensions.get('window');

const ProfileSettingsLayout = ({children, header, subHeader, breadCrumbs}) => {
  const [activeBottomTab, setActiveBottomTab] = useState(BOTTOM_TABS[1]);

  return (
    <AppMainLayout>
      <ActionBar />
      <SettingsScreenHeader
        header={header}
        subHeader={subHeader}
        breadCrumbs={breadCrumbs}
      />
      <View style={styles.container}>{children}</View>
      <View style={styles.bottomTab}>
        <FlatList
          data={BOTTOM_TABS}
          horizontal
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item) => item.title}
          renderItem={({item}) => (
            <TouchableOpacity
              key={item.title}
              disabled={item.disabled}
              onPress={() => setActiveBottomTab(item)}>
              <View
                style={[
                  styles.bottomTabItem,
                  item.disabled && {opacity: 0.4},
                  activeBottomTab?.title === item.title && {
                    borderTopWidth: 1,
                  },
                ]}>
                <Text
                  style={[
                    styles.bottomTabText,
                    activeBottomTab?.title === item.title && {
                      fontFamily: Constant.FONT_SEMI_BOLD,
                    },
                  ]}>
                  {item.title}
                </Text>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    </AppMainLayout>
  );
};

export default ProfileSettingsLayout;

const styles = StyleSheet.create({
  container: {flex: 1, paddingHorizontal: 20},
  bottomTab: {
    flexDirection: 'row',
  },
  bottomTabItem: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopColor: Constant.APP_MAIN_COLOR,
    width: width / 4,
  },
  bottomTabText: {
    textAlign: 'center',
    fontFamily: Constant.FONT_NORMAL,
    color: Constant.APP_MAIN_COLOR,
  },
});

const BOTTOM_TABS = [
  {title: 'XID', disabled: true},
  {title: 'Settings'},
  {title: 'KYC', disabled: true},
  {title: 'Brain', disabled: true},
];
