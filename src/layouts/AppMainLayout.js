import React, {useContext} from 'react';
import {StyleSheet} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {AppContext} from '../contexts/AppContextProvider';
import AppStatusBar from '../components/AppStatusBar';

const AppMainLayout = ({children, disableBlockCheck}) => {
  const {
    showBCHelper,
    isBlockSheetOpen,
    setIsBlockSheetOpen,
    isBlockCheckSend,
  } = useContext(AppContext);

  return (
    <SafeAreaView style={styles.container}>
      <AppStatusBar backgroundColor="white" barStyle={'dark-content'} />
      {children}
      {!disableBlockCheck && showBCHelper && <BlockCheck />}
    </SafeAreaView>
  );
};

export default AppMainLayout;

const styles = StyleSheet.create({
  container: {backgroundColor: '#ffffff', flex: 1},
});
