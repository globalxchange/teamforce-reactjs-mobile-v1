import React from 'react';
import {
  createStackNavigator,
  TransitionSpecs,
  CardStyleInterpolators,
  HeaderStyleInterpolators,
} from '@react-navigation/stack';
import AsyncStorageHelper from '../utilities/AsyncStorageHelper';

import LoginScreen from '../screens/LoginView/LoginScreen';
import WelcomeScreen from '../screens/LoginView/WelcomeScreen';
import MFaVerificationScreen from '../screens/LoginView/MFaVerificationScreen';
import RegisterScreen from '../screens/LoginView/RegisterScreen';
import HomeFragmentsScreen from '../screens/HomeView/HomeFragmentsScreen';
import GametimeScreen from '../screens/HomeView/GametimeScreen';
import LiveGameDetailScreen from '../screens/LiveGameDetail/LiveGameDetailScreen';
import FitnessScreen from '../screens/HomeView/FitnessScreen';
import RedHeartScreen from '../screens/HomeView/RedHeartScreen';
import TimerScreen from '../screens/HomeView/TimerScreen';
import ProfileScreen from '../screens/HomeView/ProfileScreen';
import DrawerMenuScreen from '../screens/DrawerScreen/DrawerMenuScreen';


const Stack = createStackNavigator();

const AppStackNavigator = () => {

  const config = {
    animation: 'left',
    config: {
      stiffness: 1000,
      damping: 500,
      mass: 3,
      overshootClamping: true,
      restDisplacementThreshold: 0.01,
      restSpeedThreshold: 0.01,
    },
  };

  const forFade = ({ current, next }) => {
    const opacity = Animated.add(
      current.progress,
      next ? next.progress : 0
    ).interpolate({
      inputRange: [0, 1, 2],
      outputRange: [0, 1, 0],
    });
  
    return {
      leftButtonStyle: { opacity },
      rightButtonStyle: { opacity },
      titleStyle: { opacity },
      backgroundStyle: { opacity },
    };
  };

  return (
      <Stack.Navigator initialRouteName={AsyncStorageHelper.getIsLoggedIn() === true ? "MainTabbar" : "WelcomeScreen"} 
      screenOptions={{headerShown: false}} animation={config} screenOptions={{
        headerShown: false,
        cardStyle: { backgroundColor: 'transparent' },
        cardOverlayEnabled: true,
        cardStyleInterpolator: ({ current: { progress } }) => ({
          cardStyle: {
            opacity: progress.interpolate({
              inputRange: [0, 0.5, 0.9, 1],
              outputRange: [0, 0.25, 0.7, 1],
            }),
          },
          overlayStyle: {
            opacity: progress.interpolate({
              inputRange: [0, 1],
              outputRange: [0, 0.5],
              extrapolate: 'clamp',
            }),
          },
        }),
      }}
      mode="modal">
        <Stack.Screen name="Welcome" component={WelcomeScreen} />
        <Stack.Screen name="Login" component={LoginScreen} options={{
            transitionSpec: { open: TransitionSpecs.TransitionIOSSpec, close: TransitionSpecs.TransitionIOSSpec, },
            headerStyleInterpolator: forFade,
            cardStyleInterpolator: CardStyleInterpolators.forFadeFromBottomAndroid,
            // ...TransitionPresets.ModalTransition, 
            headerStyleInterpolator: HeaderStyleInterpolators.forUIKit,}}/>
        <Stack.Screen name="MFaVerification" component={MFaVerificationScreen} />
        <Stack.Screen name="Register" component={RegisterScreen} />
        <Stack.Screen name="HomeFragments" component={HomeFragmentsScreen} options={{
            transitionSpec: { open: TransitionSpecs.TransitionIOSSpec, close: TransitionSpecs.TransitionIOSSpec, },
            headerStyleInterpolator: forFade,
            cardStyleInterpolator: CardStyleInterpolators.forFadeFromBottomAndroid,
            // ...TransitionPresets.ModalTransition, 
            headerStyleInterpolator: HeaderStyleInterpolators.forUIKit,}} />
        <Stack.Screen name="GametimeScreen" component={GametimeScreen} />
        <Stack.Screen name="FitnessScreen" component={FitnessScreen} />
        <Stack.Screen name="RedHeartScreen" component={RedHeartScreen} />
        <Stack.Screen name="TimerScreen" component={TimerScreen} />
        <Stack.Screen name="ProfileScreen" component={ProfileScreen} />
        <Stack.Screen name="DrawerMenuScreen" component={DrawerMenuScreen} />
        <Stack.Screen name="LiveGameDetailScreen" component={LiveGameDetailScreen} />
      </Stack.Navigator>
  );
};

export default AppStackNavigator;
