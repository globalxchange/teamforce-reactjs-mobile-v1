# Android Keystore Credentials

These credentials are used in conjunction with your Android keystore file to sign your app for distribution. 

## Credential Values

- Android keystore password: dd28fd17089b4b9d8738b5244db4e9f4
- Android key alias: QGhpcmVuam9zaGkwMDA3L1RlYW1Gb3JjZQ==
- Android key password: 0e70392fcbd04efdbe2795506c4f4d6d
      