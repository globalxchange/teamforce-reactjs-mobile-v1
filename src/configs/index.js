import Constant from '../utilities/Constants';


export const GX_API_ENDPOINT = 'https://comms.globalxchange.com';

export const TELLER_API_ENDPOINT = 'https://tellerapi.apimachine.com';

export const GX_CHAT_SOCKET_ENDPOINT = 'https://chatsapi.globalxchange.io';

export const GX_AUTH_URL = 'https://gxauth.apimachine.com';

export const SUPPORT_TICKET_API = 'https://nitrogen.apimachine.com';

export const AGENCY_API_URL = 'https://fxagency.apimachine.com';

export const NEW_CHAT_API = 'https://testchatsioapi.globalxchange.io';

export const NEW_CHAT_SOCKET = 'https://testsockchatsio.globalxchange.io';

export const TELLER_API2_ENDPOINT = 'https://teller2.apimachine.com';


export const supportTabs = [
    {
      title: 'Arena',
      icon: Constant.RearIC,
    },
    {
      title: 'Fitness',
      icon: Constant.FitnessIC,
      loginRequired: true,
    },
    {
      title: 'ReadHeart',
      icon: Constant.RedHeartIC,
      loginRequired: true,
    },
    {
      title: 'Timer',
      icon: Constant.TimerIC,
      loginRequired: true,
    },
    {
      title: 'Profile',
      icon: Constant.ProfileIC,
      loginRequired: true,
    },
  ];