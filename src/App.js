/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState, useRef, useContext} from 'react';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import {View} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import AsyncStorageHelper from './utilities/AsyncStorageHelper';
import Axios from 'axios';
import {GX_AUTH_URL, GX_API_ENDPOINT, COGNITO_USER_POOL_ID} from './configs';
import SplashScreen from 'react-native-splash-screen';

import AppStackNav from './navigation/AppStackNavigator';
import HomeStackNav from './navigation/HomeStackNavigation';


const App = () => {

  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [isLoded, setisLoded] = useState(false);

  const isTokenCheckRunning = useRef(false);

  useEffect( async () => {
    setisLoded(await AsyncStorageHelper.getIsLoggedIn());
  });

  useEffect(() => {
    if (!isTokenCheckRunning.current) {
      checkTokenValidity();
    }
  });

  const checkTokenValidity = async () => {
    isTokenCheckRunning.current = true;

    const token = await AsyncStorageHelper.getidToken();

    if (token) {
      Axios.post(`${GX_API_ENDPOINT}/brokerage/verify_token`, {
        token,
        userpoolid: COGNITO_USER_POOL_ID,
      })
        .then((tokenCheckResp) => {
          let user;
          if (tokenCheckResp.data) {
            user = tokenCheckResp.data;
          } else {
            user = null;
          }

          if (user !== null && Date.now() < user.exp * 1000) {
            console.log('Session valid!!');
            isTokenCheckRunning.current = false;
            setIsLoggedIn(true);
          } else {
            console.log('Invalid Session, Updating Token');
            updateToken();
          }
        })
        .catch((error) => {
          console.log('Error on verifying Token', error);
          isTokenCheckRunning.current = false;
          // setIsLoggedIn(false);
          // AsyncStorageHelper.deleteAllUserInfo();
        });
    } else {
      isTokenCheckRunning.current = false;
    }
  };

  const updateToken = async () => {
    const deviceKey = await AsyncStorageHelper.getDeviceKey();
    const refreshToken = await AsyncStorageHelper.getRefreshToken();

    if (deviceKey && refreshToken) {
      Axios.post(`${GX_AUTH_URL}/gx/user/refresh/tokens`, {
        refreshToken,
        device_key: deviceKey,
      })
        .then((resp) => {
          const {data} = resp;

          if (data.status) {
            console.log('Token Updated');
            AsyncStorageHelper.setidToken(data?.idToken || '');
            AsyncStorageHelper.setAccessToken(data?.accessToken || '');
            setIsLoggedIn(true);
          } else {
            console.log('Refresh Token is Invalid');
            setIsLoggedIn(false);
            AsyncStorageHelper.deleteAllUserInfo();
          }
        })
        .catch((error) => {
          console.log('Error on refreshing token', error);
          // setIsLoggedIn(false);
          // AsyncStorageHelper.deleteAllUserInfo();
        })
        .finally(() => {
          isTokenCheckRunning.current = false;
        });
    }
  };

  return (
    <SafeAreaProvider>
      <NavigationContainer>
        { isLoded ? (<HomeStackNav />) : (isLoggedIn ? (<HomeStackNav />) : (<AppStackNav />) )}
      </NavigationContainer>
    </SafeAreaProvider>
  );
};

export default App;
