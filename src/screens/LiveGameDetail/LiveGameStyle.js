import { StyleSheet, Platform, Dimensions } from 'react-native';
import ThemeStyle from '../../utilities/ThemeStyle';
import Constants from '../../utilities/Constants';


export const LiveGameStyle = StyleSheet.create({
    maincontainer: {
      flex: 1,
      width: '100%',
      height: '100%',
      backgroundColor: '#fff',
      justifyContent: 'center',
      alignContent: 'center',
      alignItems: 'center'
    },
    container: {
      width: '50%',
      height: '20%',
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center'
    },
    headerstyle: {
      width: '100%',
      flexDirection: 'row',
      padding: '5%',
      backgroundColor: Constants.OrangeColor,
      alignContent: 'space-between',
    },  
    titlestyle: {
      color: '#fff',
      fontSize: 20,
      paddingLeft: '5%',
      paddingEnd: '25%',
    },
    subtitlestyle: {
      color: '#fff',
      fontSize: 20,
      fontWeight: 'bold',
      margin: '2%',
    },
    sectionviewstyle: {
      backgroundColor: '#fff',
      width: '100%',
    },  
    sectiontitlestyle: {
      color: '#fff',
      fontSize: 20,
      paddingLeft: '5%',
      paddingTop: '10%',
      fontWeight: 'bold',
    },
    sectionview1: {
      fontSize: 17,
      width: '100%',
      paddingLeft: '5%',
      flexDirection: 'row',
      paddingTop: '5%',
      backgroundColor: '#E3584B'
    },
    sectionview2: {
      fontSize: 17,
      paddingLeft: '5%',
      width: '100%',
      flexDirection: 'row',
      alignContent: 'stretch',
      paddingTop: '5%',
    },
    sectionview3: {
      fontSize: 17,
      paddingLeft: '5%',
      width: '100%',
      flexDirection: 'row',
      alignContent: 'stretch',
      paddingTop: '5%',
      paddingBottom: '10%',
    },  
    Rowtitlestyle: {
      color: '#fff',
      fontSize: 17,
      textAlign: 'left',
    },
    Rowsubtitlestyle: {
      flex: 1,
      fontSize: 17,
      width: '70%',
      alignContent: 'space-around',
      textAlign: 'right',
      paddingEnd: 20
    },
    earningviewstyle: {
      width: '100%',
      height: '50%',
    },
    bottomOptionContainer: {
      flexDirection: 'row', 
      paddingHorizontal: 20, 
      paddingBottom: '10%', 
      alignContent: 'space-between', 
      width: '100%'
    },
  });

export default LiveGameStyle;