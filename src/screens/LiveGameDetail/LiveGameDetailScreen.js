import { StatusBar } from 'expo-status-bar';
import React, { Component, useState } from 'react';
import { 
    View,
    Platform, 
    useColorScheme, 
    Appearance,
    Text,
    TouchableOpacity,
    Image,
} from 'react-native';
import { SafeAreaView, SafeAreaProvider, SafeAreaInsetsContext } from 'react-native-safe-area-context';
import LiveGameStyle from './LiveGameStyle';
import ThemeStyle from '../../utilities/ThemeStyle';
import Constant from '../../utilities/Constants';
import PopupLayout from '../../layouts/PopupLayout';
import BottomSheetLayout from '../../layouts/BottomSheetLayout';

const LiveGameDetailScreen = ({navigation, isOpen, setIsOpen}) => {

    const [isCopied, setIsCopied] = useState(false);
    const [links, setLinks] = useState();

    const colorScheme = useColorScheme();
    const themeStatusBarStyle = colorScheme === 'light' ? 'dark-content' : 'light-content';
    const themeTextStyle = colorScheme === 'light' ? ThemeStyle.lightText : ThemeStyle.darkText;
    const themeContainerStyle = colorScheme === 'light' ? ThemeStyle.lightBG : ThemeStyle.darkBG;

    const getheader = () => {
        return (
            <View style={LiveGameStyle.headerstyle}>
                <Text style={LiveGameStyle.titlestyle}> Current Session </Text>
                <TouchableOpacity style={{paddingRight: '5%'}}>
                    <Text style={[LiveGameStyle.subtitlestyle]}> 3.45 Hrs </Text>
                </TouchableOpacity>
            </View>
        );
    }

    const getsectionview = () => {
        return (
            <View style={[LiveGameStyle.sectionviewstyle, themeContainerStyle]}>
                <Text style={[LiveGameStyle.sectiontitlestyle, themeTextStyle]}> Minutes </Text>
                <View style={[LiveGameStyle.sectionview1, themeContainerStyle]}>
                    <Text style={[LiveGameStyle.Rowtitlestyle, themeTextStyle]}>Checked In</Text>
                    <Text style={[LiveGameStyle.Rowsubtitlestyle, themeTextStyle]}>2:35 PM IST</Text>
                </View>
                <View style={[LiveGameStyle.sectionview2, themeContainerStyle]}>
                    <Text style={[LiveGameStyle.Rowtitlestyle, themeTextStyle]}>Checked Out</Text>
                    <Text style={[LiveGameStyle.Rowsubtitlestyle, themeTextStyle]}>2:35 PM IST</Text>
                </View>
                <View style={[LiveGameStyle.sectionview3, themeContainerStyle]}>
                    <Text style={[LiveGameStyle.Rowtitlestyle, themeTextStyle]}>Checked In</Text>
                    <Text style={[LiveGameStyle.Rowsubtitlestyle, themeTextStyle]}>2:35 PM IST</Text>
                </View>
            </View>
        );
    }

    const getearningview = () => {
        return (
            <SafeAreaProvider>
                <View style={[LiveGameStyle.sectionviewstyle, themeContainerStyle]}>
                    <Text style={[LiveGameStyle.sectiontitlestyle, themeTextStyle]}> Earnings </Text>
                    <View style={[LiveGameStyle.sectionview1, themeContainerStyle]}>
                        <Text style={[LiveGameStyle.Rowtitlestyle, themeTextStyle]}>Gross</Text>
                        <Text style={[LiveGameStyle.Rowsubtitlestyle, {'color': '#39D98A'}]}>2:35 PM IST</Text>
                    </View>
                    <View style={[LiveGameStyle.sectionview2, themeContainerStyle]}>
                        <Text style={[LiveGameStyle.Rowtitlestyle, themeTextStyle]}>Bonuses</Text>
                        <Text style={[LiveGameStyle.Rowsubtitlestyle, {'color': '#39D98A'}]}>2:35 PM IST</Text>
                    </View>
                    <View style={[LiveGameStyle.sectionview3, themeContainerStyle]}>
                        <Text style={[LiveGameStyle.Rowtitlestyle, themeTextStyle]}>Deductions</Text>
                        <Text style={[LiveGameStyle.Rowsubtitlestyle, {'color': '#E3584B'}]}>2:35 PM IST</Text>
                    </View>
                </View>
            </SafeAreaProvider>
        );
    }

    const bottomOption = () => {
        return (
            <View style={[LiveGameStyle.bottomOptionContainer, themeContainerStyle]}>
                <TouchableOpacity style={[LiveGameStyle.bottomactionstyle, {'borderColor': colorScheme === 'light' ? Constant.LightBlack : Constant.AliceBlue }]}>
                    <Image source={require('../../assets/PayslipIC.png')} style={{'height': 30, 'width': 30, resizeMode:'contain'}}/>
                    <Text style={[LiveGameStyle.subtitlestyle]}> PaySlip </Text>
                </TouchableOpacity>
                <TouchableOpacity style={[Constant.CommonStylesheet.bottomfullButton, {'borderColor': colorScheme === 'light' ? Constant.LightBlack : Constant.AliceBlue }]}>
                    <Image source={require('../../assets/CheckOutIC.png')} style={{'height': 30, 'width': 30, resizeMode:'contain'}}/>
                    <Text style={[LiveGameStyle.subtitlestyle]}> Check Out </Text>
                </TouchableOpacity>
            </View>
        );
    }

    return (
    <BottomSheetLayout 
    isOpen={isOpen} 
    onClose={() => setIsOpen(false)}
    reactToKeyboard 
    height={Constant.ScreenWidth * 1.7} >
        {getheader()}
        {getsectionview()}
        {getearningview()}
        {bottomOption()}
    </BottomSheetLayout>
    );
};

export default LiveGameDetailScreen;