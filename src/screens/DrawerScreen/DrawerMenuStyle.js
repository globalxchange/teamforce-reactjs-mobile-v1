import { StyleSheet } from 'react-native';

import ThemeStyle from '../../utilities/ThemeStyle';
import Constant from '../../utilities/Constants';


const DrawerMenuStyle = StyleSheet.create({
  maincontainer: {
    alignContent: 'center',
    flexDirection: 'column',
    width: '100%',
    height: '100%',
  },
  Backcontainer: {
    height: 'auto',
    margin: '5%',
    width:'10%',
  },
  optionalcontainer: {
    flex: 1,
    width: '100%',
    paddingTop: '10%',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  Optionbtn: {
    width: '100%',
    height: '12%',
    alignContent: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoutbtn: {
    width: '60%',
    height: '15%',
    marginTop: '10%',
    marginBottom: '3%',
    backgroundColor: Constant.OrangeColor,
    alignContent: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  }
  });

  export default DrawerMenuStyle;