import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import { 
  View,
  Text,
  TouchableOpacity,
  Platform,
  useColorScheme,
  ScrollView,
  Dimensions,
  Image
} from 'react-native';
import { SafeAreaProvider, SafeAreaView, useSafeAreaInsets}from 'react-native-safe-area-context';
import ThemeStyle from '../../utilities/ThemeStyle';
import AsyncStorageHelper from '../../utilities/AsyncStorageHelper';
import Constant from '../../utilities/Constants'
import DrawerMenuStyle from './DrawerMenuStyle';
import Userprofile from '../../components/ProfileAvatar';

import AppLaunch from '../../App';

const DrawerMenuScreen = ({navigation, selectedIndex}) => {

  const colorScheme = useColorScheme();
  const themeStatusBarStyle = colorScheme === 'light' ? 'dark-content' : 'light-content';
  const themeTextStyle = colorScheme === 'light' ? ThemeStyle.lightText : ThemeStyle.darkText;
  const themeContainerStyle = colorScheme === 'light' ? ThemeStyle.lightBG : ThemeStyle.darkBG;

  const [SelctIndex, setSelectIndex] = useState(selectedIndex);
  const [isLoggedIn, setisLoggedIn] = useState(false);
  
  useEffect( async () => {
    setisLoggedIn(await AsyncStorageHelper.getIsLoggedIn());
  });

  const Backbutton = () => {
    return ( <TouchableOpacity style={[DrawerMenuStyle.Backcontainer, themeContainerStyle]} onPress={()=>{handleBack()}}>
        <Image source={require('../../assets/CloseIC.png')} style={{'height': 30, 'width': 40}}/>
      </TouchableOpacity>
      );
  }

  const handleBack = () => {
    if (navigation.canGoBack()) {
      navigation.goBack();
    } else {
      BackHandler.exitApp();
    }
    return true;
  };

  const handlelogin = () => {
    // if (AsyncStorageHelper.removeLoginSession()) {
    //   <AppLaunch />
    // }
    if (AsyncStorageHelper.removeLoginSession()) {
      navigation.replace('Welcome', {screen: 'AppStackNav'});
    };
  }

  return (
    <SafeAreaView style={[DrawerMenuStyle.maincontainer, themeContainerStyle]}>
      <View style={[DrawerMenuStyle.maincontainer, themeContainerStyle]}>
        {Backbutton()}
        <Userprofile />
        <View style={[DrawerMenuStyle.optionalcontainer, themeContainerStyle]}>
            <TouchableOpacity style={DrawerMenuStyle.Optionbtn} onPress={() => setSelectIndex(0)} >
               <Text style={[{'fontSize': 25, 'fontWeight': (SelctIndex === 0 || SelctIndex == null) ? '500' : '200'}, themeTextStyle]}> Play </Text>
            </TouchableOpacity>
            <TouchableOpacity style={DrawerMenuStyle.Optionbtn} onPress={() => setSelectIndex(1)} >
               <Text style={[{'fontSize': 25, 'fontWeight': SelctIndex === 1 ? '500' : '200'}, themeTextStyle]}> Train </Text>
            </TouchableOpacity>
            <TouchableOpacity style={DrawerMenuStyle.Optionbtn} onPress={() => setSelectIndex(2)} >
               <Text style={[{'fontSize': 25, 'fontWeight': SelctIndex === 2 ? '500' : '200'}, themeTextStyle]}> Tools </Text>
            </TouchableOpacity>
            <TouchableOpacity style={DrawerMenuStyle.Optionbtn} onPress={() => setSelectIndex(3)} >
               <Text style={[{'fontSize': 25, 'fontWeight': SelctIndex === 3 ? '500' : '200'}, themeTextStyle]}> Meet </Text>
            </TouchableOpacity>
            <TouchableOpacity style={DrawerMenuStyle.logoutbtn} onPress={() => handlelogin()}>
               <Text style={{'fontSize': 25, 'fontWeight': '500', 'color': '#fff'}}> {isLoggedIn ? 'Logout' : 'Login'} </Text>
            </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default DrawerMenuScreen;