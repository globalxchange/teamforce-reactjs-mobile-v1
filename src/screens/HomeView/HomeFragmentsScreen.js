import { StatusBar } from 'expo-status-bar';
import React, {useEffect, useState, useRef, useContext} from 'react';
import { 
    View, 
    Platform, 
    useColorScheme, 
    Appearance,
    Image,
    TouchableOpacity,
    Text,
} from 'react-native';
import { SafeAreaProvider, SafeAreaView} from 'react-native-safe-area-context';
import HomeFragmentStyle from './HomeFragmentStyle';

import ThemeStyle from '../../utilities/ThemeStyle';
import Constant from '../../utilities/Constants'

import ActionBar from '../../components/ActionBar';

import GametimeScreen from './GametimeScreen';
import FitnessScreen from './FitnessScreen';
import RedHeartScreen from './RedHeartScreen';
import TimerScreen from './TimerScreen';
import ProfileScreen from './ProfileScreen';


const iconMap = {
    Gametime: Constant.RearIC,
    Fitness: Constant.FitnessIC,
    CenterTab: Constant.RedHeartIC,
    Timer: Constant.TimerIC,
    Profile: Constant.ProfileIC,
  };

const HomeFragmentsScreen = ({navigation}) => {

    const colorScheme = useColorScheme();
    const themeStatusBarStyle = colorScheme === 'light' ? 'dark-content' : 'light-content';
    const themeTextStyle = colorScheme === 'light' ? ThemeStyle.lightText : ThemeStyle.darkText;
    const themeContainerStyle = colorScheme === 'light' ? ThemeStyle.lightBG : ThemeStyle.darkBG;

    const [isSelectedTab, setisSelectedTab] = useState(0);

    const setupview = () => {
        if (isSelectedTab === 0) {
            return <GametimeScreen navigation={navigation}/>
        }
        else if (isSelectedTab === 1) {
            return <FitnessScreen navigation={navigation}/>
        }
        else if (isSelectedTab === 2) {
            return <RedHeartScreen navigation={navigation}/>
        }
        else if (isSelectedTab === 3) {
            return <TimerScreen navigation={navigation}/>
        }
        else {
            return <ProfileScreen />
        }
    }

    const BottomBar = () => {
        return (
            <>
            <View style={[HomeFragmentStyle.bottomtabs, {'borderTopColor': colorScheme === 'light' ? Constant.LightBlack : Constant.AliceBlue }]}>
                <TouchableOpacity style={HomeFragmentStyle.action} onPress={ () => setisSelectedTab(0)}>
                    <Image source={iconMap.Gametime} style={HomeFragmentStyle.tabstyle}/>
                    <Text style={{'color': isSelectedTab === 0 ? Constant.OrangeColor : Constant.AliceBlue, 'padding': 2}} >Arena</Text>
                </TouchableOpacity> 

                <TouchableOpacity style={HomeFragmentStyle.action} onPress={() => setisSelectedTab(1)}>
                    <Image source={iconMap.Fitness} style={HomeFragmentStyle.tabstyle}/>
                    <Text style={{'color': isSelectedTab === 1 ? Constant.OrangeColor : Constant.AliceBlue, 'padding': 2}} >Fitness</Text>
                </TouchableOpacity>                     

                <TouchableOpacity style={HomeFragmentStyle.action} onPress={() => setisSelectedTab(2)}>
                    <View style={{'backgroundColor': Constant.OrangeColor, 'padding': 12, 'borderRadius': 100, }}>
                        <Image source={iconMap.CenterTab} style={HomeFragmentStyle.centerstyle}/>
                    </View>
                </TouchableOpacity> 

                <TouchableOpacity style={HomeFragmentStyle.action} onPress={() => setisSelectedTab(3)}>
                    <Image source={iconMap.Timer} style={HomeFragmentStyle.tabstyle}/>
                    <Text style={{'color': isSelectedTab === 3 ? Constant.OrangeColor : Constant.AliceBlue, 'padding': 2}} >Timer</Text>
                </TouchableOpacity> 

                <TouchableOpacity style={HomeFragmentStyle.action} onPress={() => setisSelectedTab(4)}>
                    <Image source={iconMap.Profile} style={HomeFragmentStyle.tabstyle}/>
                    <Text style={{'color': isSelectedTab === 4 ? Constant.OrangeColor : Constant.AliceBlue, 'padding': 2}} >Profile</Text>
                </TouchableOpacity> 
            </View>
            </>
        );
    }

    return (
        <SafeAreaView style={[HomeFragmentStyle.maincontainer , themeContainerStyle]}>
            <StatusBar barstyles={themeStatusBarStyle}/>
            <ActionBar navigation={navigation}/>
            <View style={[HomeFragmentStyle.container]}>
                {setupview()}
            </View>
            {BottomBar()}
        </SafeAreaView>
    );
};

export default HomeFragmentsScreen;