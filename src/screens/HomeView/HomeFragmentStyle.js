import { StyleSheet, Platform, Dimensions } from 'react-native';
import Constant from '../../utilities/Constants';


export const HomeFragmentStyle = StyleSheet.create({
    maincontainer: {
      flex: 1,
      flexDirection: 'column',
      width: '100%',
      height: '100%',
      backgroundColor: '#fff',
      justifyContent: 'center',
      alignContent: 'center',
      alignItems: 'center'
    },
    topcontainer: {
      width: '100%',
      height: '10%',
      backgroundColor: '#FFEBCD',
      alignItems: 'center',
      justifyContent: 'center'
    },
    container: {
      width: '100%',
      height: '81%',
      backgroundColor: '#FFEBCD',
      alignItems: 'center',
      justifyContent: 'center',
    },
    bottomtabs: {
      width: '100%',
      height: '10%',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-around',
      borderTopWidth: 1,
    },
    tabstyle: {
      width: '55%',
      height: '40%',
      margin: '0%',
      alignItems: 'center',
      justifyContent: 'center',
      marginBottom: 5,
    },
    centerstyle: {
      width: '50%',
      height: '50%',
      padding: 20,
    },
    action: {
      alignItems: 'center',
      justifyContent: 'center',
      resizeMode: 'stretch',
    }
  });

export default HomeFragmentStyle;