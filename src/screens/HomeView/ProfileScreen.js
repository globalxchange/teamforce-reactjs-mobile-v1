import { StatusBar } from 'expo-status-bar';
import { NavigationContainer } from '@react-navigation/native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import React, { Component } from 'react';
import { 
    View, 
    Platform, 
    useColorScheme, 
    Appearance,
    Text,
    TouchableOpacity,
} from 'react-native';
import ProfileStyle from './ProfileStyle';
import ThemeStyle from '../../utilities/ThemeStyle';
import AsyncStorageHelper from '../../utilities/AsyncStorageHelper';

import AppStackNav from '../../navigation/AppStackNavigator';

const ProfileScreen = ({navigation}) => {

    const colorScheme = useColorScheme();
    const themeStatusBarStyle = colorScheme === 'light' ? 'dark-content' : 'light-content';
    const themeTextStyle = colorScheme === 'light' ? ThemeStyle.lightText : ThemeStyle.darkText;
    const themeContainerStyle = colorScheme === 'light' ? ThemeStyle.lightBG : ThemeStyle.darkBG;

    const generatestring = (length) => {
        var result           = [];
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
          result.push(characters.charAt(Math.floor(Math.random() * 
     charactersLength)));
       }
       console.log(result.join(''))
       return result.join('');
    }

    return (
    <View style={[ProfileStyle.maincontainer, themeContainerStyle]}>
        <Text style={themeTextStyle}>{generatestring(150)}</Text>
    </View>
    );
};

export default ProfileScreen;