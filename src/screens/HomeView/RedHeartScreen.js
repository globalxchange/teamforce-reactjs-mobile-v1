import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { 
    View,
    Platform, 
    useColorScheme, 
    Appearance,
    Text
} from 'react-native';
import RedHeartStyle from './RedHeartStyle';
import ThemeStyle from '../../utilities/ThemeStyle';

const RedHeartScreen = ({navigation}) => {

    const colorScheme = useColorScheme();
    const themeStatusBarStyle = colorScheme === 'light' ? 'dark-content' : 'light-content';
    const themeTextStyle = colorScheme === 'light' ? ThemeStyle.lightText : ThemeStyle.darkText;
    const themeContainerStyle = colorScheme === 'light' ? ThemeStyle.lightBG : ThemeStyle.darkBG;

    const generatestring = (length) => {
        var result           = [];
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
          result.push(characters.charAt(Math.floor(Math.random() * 
     charactersLength)));
       }
       console.log(result.join(''))
       return result.join('');
    }

    return (
    <View style={[RedHeartStyle.maincontainer, themeContainerStyle]}>
        <Text style={themeTextStyle}>{generatestring(150)}</Text>
    </View>
    );
};

export default RedHeartScreen;