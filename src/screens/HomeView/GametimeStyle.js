import { StyleSheet, Platform, Dimensions } from 'react-native';
import Constants from '../../utilities/Constants';
import ThemeStyle from '../../utilities/ThemeStyle';

const {width} = Dimensions.get('window');

export const GametimeStyle = StyleSheet.create({
    maincontainer: {
      flex: 1,
      width: '100%',
      height: '100%',
    },
    container: {
      width: '50%',
      height: '20%',
      justifyContent: 'center',
      alignContent: 'center',
      alignItems: 'center'
    },
    headerstyle: {
      flex: 1,
      width: '100%',
      flexDirection: 'row',
      paddingTop: '10%',
      alignContent: 'space-between',
    },  
    titlestyle: {
      color: Constants.OrangeColor,
      fontSize: 30,
      paddingLeft: '5%',
      paddingEnd: '30%',
    },
    newstyle: {
      flex: 1,
      color: Constants.OrangeColor,
      fontSize: 15,
      margin: '1.5%',
      borderColor: '#000',
      borderWidth: 0.2,
      marginRight: '5%',
      justifyContent: 'center',
      alignContent: 'center',
      alignItems: 'center',
    },
    livegamestyle: {
      paddingTop: '10%',
      width: '100%',
    },
    livettitlestyle: {
      color: Constants.OrangeColor,
      fontSize: 20,
      paddingLeft: '6%',
      paddingBottom: '5%'
    },
    livecontentcellstyle: {
      width: '100%',
      marginLeft: '5%',
      marginBottom: '2%',
      flexDirection: 'row',
    },
    livegamecell: {
      borderWidth: 0.2,
      height: 100,
      width: width / 2.2,
      marginRight: 12,
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0,
      shadowRadius: 2,
      elevation: 5,
    },
    recentcontentcellstyle: {
      height: '100%',
      marginLeft: '5%',
      marginRight: '5%',
      flexDirection: 'column',
      paddingBottom: '5%',
    },
    recentgamecell: {
      borderWidth: 0.2,
      height: 100,
      width: '100%',
      marginBottom: '5%',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0,
      shadowRadius: 2,
      elevation: 5,
    }
  });

export default GametimeStyle;