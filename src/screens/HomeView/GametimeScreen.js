import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import { 
    View, 
    Platform, 
    useColorScheme, 
    Appearance,
    Text,
    ScrollView,
    TouchableOpacity,
    FlatList
} from 'react-native';
import GametimeStyle from './GametimeStyle';
import ThemeStyle from '../../utilities/ThemeStyle';
import Constant from '../../utilities/Constants';
import { SafeAreaProvider, SafeAreaView} from 'react-native-safe-area-context';
import LiveGameDetails from '../LiveGameDetail/LiveGameDetailScreen';

const GametimeScreen = ({navigation}) => {

    const colorScheme = useColorScheme();
    const themeStatusBarStyle = colorScheme === 'light' ? 'dark-content' : 'light-content';
    const themeTextStyle = colorScheme === 'light' ? ThemeStyle.lightText : ThemeStyle.darkText;
    const themeContainerStyle = colorScheme === 'light' ? ThemeStyle.lightBG : ThemeStyle.darkBG;
    const [isOpen, setIsOpen] = useState(false);

    const gameheader = () => {
        return (
            <View style={GametimeStyle.headerstyle}>
                <Text style={GametimeStyle.titlestyle}> GAMETIME </Text>
                <TouchableOpacity style={[GametimeStyle.newstyle, {'borderColor': colorScheme === 'light' ? Constant.LightBlack : Constant.AliceBlue }]}>
                    <Text style={[{padding: 1, justifyContent: 'center', alignContent: 'center', alignItems: 'center'}, themeTextStyle]}> {'+ New'} </Text>
                </TouchableOpacity>
            </View>
        );
    }

    const setuplivegame = () => {
        return (
            <View style={GametimeStyle.livegamestyle}>
                <Text style={GametimeStyle.livettitlestyle}> Live Games</Text>
                {setuplivegamecontentcell()}
            </View>
        );
    }

    const setuplivegamecontentcell = () => {
        return (
            <View style={GametimeStyle.livecontentcellstyle}>
                {setuplivegamecell()}
                {setuplivegamecell()}
                {setuplivegamecell()}
            </View>
        );
    }
    
    const setuplivegamecell = () => {
        return (
            <TouchableOpacity 
            style={[GametimeStyle.livegamecell, {shadowColor: colorScheme === 'light' ? Constant.LightBlack : Constant.AliceBlue, borderColor: colorScheme === 'light' ? Constant.LightBlack : Constant.AliceBlue}]} 
            onPress={() => {setIsOpen(true)}}>
            </TouchableOpacity>
        );
    }

    const setuprecentgame = () => {
        return (
            <View style={GametimeStyle.livegamestyle}>
                <Text style={GametimeStyle.livettitlestyle}> Recent Games</Text>
                {setuprecentgamecontentcell()}
            </View>
        );
    }

    const setuprecentgamecontentcell = () => {
        return (
            <View style={GametimeStyle.recentcontentcellstyle}>
                {setuprecentgamecell()}
                {setuprecentgamecell()}
                {setuprecentgamecell()}
            </View>
        );
    }
    
    const setuprecentgamecell = () => {
        return (
            <TouchableOpacity style={[GametimeStyle.recentgamecell, {shadowColor: colorScheme === 'light' ? Constant.LightBlack : Constant.AliceBlue, borderColor: colorScheme === 'light' ? Constant.LightBlack : Constant.AliceBlue}]}>

            </TouchableOpacity>
        );
    }

    return (
        <SafeAreaProvider style={[GametimeStyle.maincontainer, themeContainerStyle]}>
            <ScrollView >
                {gameheader()}
                {setuplivegame()}
                {setuprecentgame()}
            </ScrollView>
            <LiveGameDetails isOpen={isOpen} setIsOpen={setIsOpen}/>
        </SafeAreaProvider>
    );
};

export default GametimeScreen;