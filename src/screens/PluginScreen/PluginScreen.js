import { StatusBar } from 'expo-status-bar';
import React, { Component, useState } from 'react';
import { 
    View,  
    Platform, 
    useColorScheme, 
    Text,
    TouchableOpacity,
    Image,
    ScrollView
} from 'react-native';
import { SafeAreaView, SafeAreaProvider } from 'react-native-safe-area-context';
import PluginStyle from './PluginStyle';
import ThemeStyle from '../../utilities/ThemeStyle';
import BottomSheetLayout from '../../layouts/BottomSheetLayout';
import Constant from '../../utilities/Constants';
import TMheaderView from '../../components/TMheaderView';
import TransferScreen from './TransferScreens/TransferingScreen';

const PluginScreen = ({navigation, isOpen, setIsOpen}) => {

    const colorScheme = useColorScheme();
    const themeStatusBarStyle = colorScheme === 'light' ? 'dark-content' : 'light-content';
    const themeTextStyle = colorScheme === 'light' ? ThemeStyle.lightText : ThemeStyle.darkText;
    const themeContainerStyle = colorScheme === 'light' ? ThemeStyle.lightBG : ThemeStyle.darkBG;
    const [isOpenConnect, setIsOpenConnect] = useState(false);


    const setuprecentgamecontentcell = () => {
        return (
            <View style={PluginStyle.plugincontentcellstyle}>
                {setuprecentgamecell(Constant.BlockckcheckIcon, 0)}
                {setuprecentgamecell(Constant.ConnectIcon, 1)}
                {setuprecentgamecell(Constant.SupportIcon, 2)}
                {setuprecentgamecell(Constant.MoneyMarketsIcon, 3)}
                {setuprecentgamecell(Constant.RefreshIcon, 4)}
                {setuprecentgamecell(Constant.ChatsIOIcon, 5)}
            </View>
        );
    }
    
    const setuprecentgamecell = (imagename, index) => {
        return (
            <TouchableOpacity style={[PluginStyle.plagincell, {shadowColor: colorScheme === 'light' ? Constant.LightBlack : Constant.AliceBlue, borderColor: colorScheme === 'light' ? Constant.LightBlack : Constant.AliceBlue}]} onPress={() => managecellaction(index)}>
                <Image style={PluginStyle.appIcon} resizeMode="center" source={imagename} />
            </TouchableOpacity>
        );
    }

    const managecellaction = (index) => {
        if (index == 0) {

        }
        else if (index == 1) {
            setIsOpenConnect(true)
        }
        else if (index == 2) {

        }
        else if (index == 3) {
            
        }
        else if (index == 4) {
            
        }
        else {
            
        }
        setIsOpen(false)
    }

    const getAppUpdate = () => {

    }

    return (
        <>
        <BottomSheetLayout 
        isOpen={isOpen} 
        onClose={() => setIsOpen(false)}
        reactToKeyboard 
        height={Constant.ScreenWidth * 1.6} >
            <TMheaderView BGcolor={themeContainerStyle}/>
            <ScrollView style={[{'flex': 1, 'width': '100%',}]}>
                {setuprecentgamecontentcell()}
            </ScrollView>
            <TouchableOpacity style={PluginStyle.pluginappupdate} onPress={() => getAppUpdate()}>
                <Image style={{'width': 20, 'height': 20, 'marginRight': 5}} source={Constant.AppUpdateIC} />
                <Text style={{'fontSize': 20, 'fontWeight': '500', 'color': '#fff', 'alignContent': 'center', 'justifyContent': 'center', 'alignItems': 'center'}}> App Updates </Text>
            </TouchableOpacity>
        </BottomSheetLayout>
        <TransferScreen navigation={navigation} isOpen={isOpenConnect} setIsOpen={setIsOpenConnect}/>
        </>
    );
};

export default PluginScreen;