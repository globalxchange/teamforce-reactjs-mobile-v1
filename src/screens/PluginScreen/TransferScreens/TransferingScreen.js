import { StatusBar } from 'expo-status-bar';
import React, { Component, useState } from 'react';
import { 
    View,  
    Platform, 
    useColorScheme, 
    Text,
    TouchableOpacity,
    Image,
    ScrollView
} from 'react-native';
import { SafeAreaView, SafeAreaProvider } from 'react-native-safe-area-context';
import Axios from 'axios';
import TransferingStyle from './TransferingStyle';
import ThemeStyle from '../../../utilities/ThemeStyle';
import BottomSheetLayout from '../../../layouts/BottomSheetLayout';
import Constant from '../../../utilities/Constants';
import TMheaderView from '../../../components/TMheaderView';
import TransactionHeader from '../../../components/TransactionHeader';
import Loadinganimation from '../../../utilities/LoadingAnimation';
import FromAppScreen from '../../TransferringView/FromAppScreen';


const TransferingScreen = ({navigation, isOpen, setIsOpen, ActionImage}) => {

    const colorScheme = useColorScheme();
    const themeStatusBarStyle = colorScheme === 'light' ? 'dark-content' : 'light-content';
    const themeTextStyle = colorScheme === 'light' ? ThemeStyle.lightText : ThemeStyle.darkText;
    const themeContainerStyle = colorScheme === 'light' ? ThemeStyle.lightBG : ThemeStyle.darkBG;
    const [isLoader, setIsLoader] = useState(false)
    const [isTransfer, setIsTransfer] = useState(false)
    const [isTransferOption, setIsTransferOption] = useState('None')

    const Showloder = () => {
        return <View style={[ThemeStyle.loadingAnimation, themeContainerStyle]}>
            <Loadinganimation/>
        </View>;
    }

    const viewcontainer = () => {
        return (
            <View style={[TransferingStyle.maincontainer, themeContainerStyle]} >
                <TMheaderView BGcolor={{backgroundColor: Constant.OrangeColor}} IsSingle={true} SingleImage={Constant.ConnectWhiteIC}/>
                <TransactionHeader IsSend={true} IsToCurrency={false} IsReciepent={false} IsAmount={false}/>
                <Text style={[TransferingStyle.headtitle, themeTextStyle]}>I Am Transferring</Text>
                <TouchableOpacity style={[TransferingStyle.optioncell, {shadowColor: colorScheme === 'light' ? Constant.LightBlack : Constant.AliceBlue, borderColor: colorScheme === 'light' ? Constant.LightBlack : Constant.AliceBlue}]} onPress={() => {setIsTransferOption('Vault')}} >
                    <Image style={{width: 25, height: 25, marginRight: 20, margin: 20}} source={Constant.WalletsIC} />
                    <Text style={[TransferingStyle.optiontitle, themeTextStyle]}> Between My Vaults </Text>
                </TouchableOpacity>
                <TouchableOpacity style={[TransferingStyle.optioncell, {shadowColor: colorScheme === 'light' ? Constant.LightBlack : Constant.AliceBlue, borderColor: colorScheme === 'light' ? Constant.LightBlack : Constant.AliceBlue}]} onPress={() => {setIsTransferOption('GXUser')}}>
                    <Image style={{width: 25, height: 25, marginRight: 20, margin: 20}} source={Constant.AnotherGXIC} />
                    <Text style={[TransferingStyle.optiontitle, themeTextStyle]}> To Another GX User </Text>
                </TouchableOpacity>
                <TouchableOpacity style={[Constant.CommonStylesheet.centerButton]} onPress={() => presentOption()}>
                    <Text style={[TransferingStyle.optiontitle]}> Proceed To Select Currency </Text>
                </TouchableOpacity>
            </View>
        );
    }

    const presentOption = () => {
        if (isTransferOption === 'Vault') {
            setIsTransfer(true)
        }
        else if (isTransferOption === 'GXUser') {
            setIsTransfer(true)
        }
        else {
            setIsTransfer(false)
        }
        setIsOpen(false)
    }

    return (
        <>
            <BottomSheetLayout 
            isOpen={isOpen} 
            onClose={() => setIsOpen(false)}
            reactToKeyboard 
            height={Constant.ScreenWidth * 1.5} >
                { isLoader ? Showloder() : viewcontainer()}
            </BottomSheetLayout>
            <FromAppScreen navigation={navigation} TransferOption={isTransferOption} isOpen={isTransfer} setIsOpen={setIsTransfer}/>
        </>
    );
};

export default TransferingScreen;