import { StyleSheet, Platform, Dimensions } from 'react-native';
import ThemeStyle from '../../../utilities/ThemeStyle';
import Constant from '../../../utilities/Constants';

export const TransferingStyle = StyleSheet.create({
  maincontainer: {
    flex: 1,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  container: {
    width: '50%',
    height: '20%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  pluginappupdate: {
    width: '100%',
    height: '10%',
    marginTop: '5%',
    backgroundColor: Constant.OrangeColor,
    alignContent: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  plugincontentcellstyle: {
    flex: 1,
    paddingTop: 30,
    marginLeft: '8%',
    marginRight: '8%',
    flexDirection: 'column',
    marginBottom: '15%',
  },
  plagincell: {
    flex: 1,
    borderWidth: 0.2,
    height: '50%',
    width: '100%',
    marginBottom: '10%',
    borderRadius: 20,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0,
    shadowRadius: 2,
  },
  appIcon: {
    height: 70,
    width: '100%',
    padding: 15,
  },
  headtitle: {
    fontSize: 35,
    marginTop: '12%',
    marginBottom: '10%',
  },
  optioncell: {
    flex: 1,
    flexDirection: 'row',
    borderWidth: 0.2,
    height: '50%',
    marginBottom: '5%',
    borderRadius: 20,
    alignItems: 'center',
    paddingLeft: '10%',
    paddingRight: '10%',
  },
  optiontitle: {
    fontSize: 20,
    color: '#fff',
  }
  });

export default TransferingStyle;