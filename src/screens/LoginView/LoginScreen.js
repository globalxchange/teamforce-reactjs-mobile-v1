import { StatusBar } from 'expo-status-bar';
import React, {useState} from 'react';
import { 
  View,
  Text,
  TouchableOpacity,
  Platform,
  useColorScheme,
  TextInput,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  Image,
} from 'react-native';
import { SafeAreaProvider, SafeAreaView} from 'react-native-safe-area-context';
import loginstyles from './LoginStyle';
import ThemeStyle from '../../utilities/ThemeStyle';
import Axios from 'axios';
import Loadinganimation from '../../utilities/LoadingAnimation';
import AsyncStorageHelper from '../../utilities/AsyncStorageHelper';
import Constant from '../../utilities/Constants'

const LoginScreen = ({navigation}) => {

  const colorScheme = useColorScheme();
  const themeStatusBarStyle = colorScheme === 'light' ? 'dark-content' : 'light-content';
  const themeTextStyle = colorScheme === 'light' ? ThemeStyle.lightText : ThemeStyle.darkText;
  const themeContainerStyle = colorScheme === 'light' ? ThemeStyle.lightBG : ThemeStyle.darkBG;

  const [Email, setEmail] = useState("");
  const [Password, setPassword] = useState("");
  const [IsEmail, setIsEmail] = useState(false);
  const [ValidEmail, setValidEmail] = useState(false);
  const [IsPass, setIsPass] = useState(false);
  const [ValidPass, setValidPass] = useState(false);
  const [IsContain8, setIsContain8] = useState(false);
  const [IsContaindigit, setIsContaindigit] = useState(false);
  const [IsContainSpecial, setIsContainSpecial] = useState(false);
  const [IsContainUpper, setIsContainUpper] = useState(false);
  const [IsContainCharacter, setIsContainCharacter] = useState(false);
  const [IsLoader, setIsLoader] = useState(false);

  const validateEmail = (text) => {
    setEmail(text)
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text.trim())) {
      setValidEmail(true)
      console.log("Valid Email");
    }
    else {
      setValidEmail(false)
      console.log("Not Valid Email");
    }
  }

  const validatePassword = (text) => {
    setPassword(text)
    if (CheckContainCharacter(text) && CheckContainUppercase(text) && CheckContainSpecialCharacter(text) && CheckContainDigit(text) && CheckMinimumcharacter(text)) {
      setValidPass(true)
      setIsPass(true)
    }
    else {
      if (CheckContainCharacter(text)) {
        setIsContainCharacter(true)
      }
      if (CheckContainUppercase(text)) {
        setIsContainUpper(true)
      }
      if (CheckContainSpecialCharacter(text)) {
        setIsContainSpecial(true)
      }
      if (CheckContainDigit(text)) {
        setIsContaindigit(true)
      }
      if (CheckMinimumcharacter(text)) {
        setIsContain8(true)
      }
      setValidPass(false)
      setIsPass(false)
    }
  }

  const CheckProperPass = (text) => {
    let reg = /^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{7,}$/
    if (reg.test(text.trim()) === false) {
      console.log("Not Proper password");
      return false
    }
    else {
      console.log("Proper password");
      return true
    } 
  }

  const CheckContainCharacter = (text) => {
    let reg = /(?=.*[a-zA-Z])/
    if (reg.test(text.trim()) === false) {
      console.log("Not Contain character");
      setIsContainCharacter(false)
      return false
    }
    else {
      console.log("Contain character");
      setIsContainCharacter(true)
      return true
    }
  }

  const CheckContainDigit = (text) => {
    let reg = /(?=.*[0-9])/
    if (reg.test(text.trim()) === false) {
      console.log("Not Contain Digit");
      setIsContaindigit(false)
      return false
    }
    else {
      console.log("Contain Digit");
      setIsContaindigit(true)
      return true
    }
  }

  const CheckContainSpecialCharacter = (text) => {
    let reg = /(?=.[!@#$%^&])/
    if (reg.test(text.trim()) === false) {
      console.log("Not Contain special character");
      setIsContainSpecial(false)
      return false
    }
    else {
      console.log("Contain Special character");
      setIsContainSpecial(true)
      return true
    }
  }

  const CheckContainUppercase = (text) => {
    let reg = /(?=.*[A-Z])/
    if (reg.test(text.trim()) === false) {
      console.log("Not Contain Upper case");
      setIsContainUpper(false)
      return false
    }
    else {
      console.log("Contain Upper case");
      setIsContainUpper(true)
      return true
    }
  }

  const CheckMinimumcharacter = (text) => {
    let reg = /(?=.{7,})/
    console.log("password contain like this");
    console.log(text.trim());
    if (reg.test(text.trim()) === false) {
      console.log("Not Minimum 8");
      setIsContain8(false)
      return false
    }
    else {
      console.log("Contain Minimum 8");
      setIsContain8(true)
      return true
    }
  }

  const validate = () => {
    if (ValidEmail) {
      if (ValidPass) {
        setIsPass(true)
        setIsEmail(true)
        if (ValidEmail && ValidPass) {
          servicesCall()
        }
      }
      else {
        setIsEmail(true)
        setIsPass(false)
      }
    }
    else {
        setIsEmail(false)
        setIsPass(false)
    }
    forceUpdateHandler
  }

  const servicesCall = async () => {
      setIsLoader(true)
      //POST json
      var dataToSend = {email: Email, password: Password};
      Axios.post(Constant.loginURL, dataToSend)
      .then((responseJson) => {
        const {data} = responseJson;
        if (data.status && responseJson.request.status === 200) {
          AsyncStorageHelper.setLoginSession(data)
          if (data.mfa) {
            
          } else {
            navigation.replace('HomeFragments', {screen: 'HomeFragmentsScreen'});
          }
        } else if (responseJson.request.status === 400 || responseJson.request.status === 401) {
          
        } else {
          
        }
        setIsLoader(false)
      })
      .catch((error) => {
        if (error.request.status === 401) {
          /*
           * The request was made but no response was received, `error.request`
           * is an instance of XMLHttpRequest in the browser and an instance
           * of http.ClientRequest in Node.js
           */
          console.log(error.request);
        } else {
          // Something happened in setting up the request and triggered an Error
          console.log('Error', error.message);
        }
        console.log(error);
        setIsLoader(false)
      });
  }

  const servicesCall2 = () => {
    if (ValidEmail && ValidPass) {
      setIsLoader(true)
      console.log("Calling login api with params");
      //POST json
      var dataToSend = {email: Email, password: Password};
      //making data to send on server
      var formBody = [];
      for (var key in dataToSend) {
        var encodedKey = encodeURIComponent(key);
        var encodedValue = encodeURIComponent(dataToSend[key]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      //POST request
      fetch('https://gxauth.apimachine.com/gx/user/login', {
        method: 'POST', //Request Type
        body: formBody, //post body
        headers: {
          'Content-Type':  'application/x-www-form-urlencoded;charset=UTF-8',
        },
      })
      .then((response) => response.json())
      //If response is in json then in success
      .then((responseJson) => {
        const {data} = responseJson;
        alert(JSON.stringify(data));
        console.log(data);
        setIsLoader(false)
      })
      //If response is not in json then in error
      .catch((error) => {
        alert(JSON.stringify(error));
        console.error(error);
        setIsLoader(false)
      });

      // const deviceKey = await AsyncStorageHelper.getDeviceKey();
      // console.log('================= Get Devicekey from async storage =================', deviceKey);
      // const session = await AsyncStorageHelper.getLoginession();
      // console.log('================= Get LoginSession from async storage =================', session);
      // const emails = await AsyncStorageHelper.getLoginEmail();
      // console.log('================= Get Email from async storage =================', emails);
      // const idtoken = await AsyncStorageHelper.getidToken();
      // console.log('================= Get idToken from async storage =================', idtoken);
      // const accesstoken = await AsyncStorageHelper.getAccessToken();
      // console.log('================= Get AccessToken from async storage =================', accesstoken);
      // const refereshtoken = await AsyncStorageHelper.getRefreshToken();
      // console.log('================= Get RefreshToken from async storage =================', refereshtoken);
    }
  }

  const forceUpdateHandler = () => {
    forceUpdate();
  };

  const displayPassMessage = () => {
    if (IsEmail) {
        return <View style={loginstyles.validationbox} > 
          <Text style={[IsContainUpper ? {color: Constant.OrangeColor} : {color: Constant.LightGrayColor}, loginstyles.validationtext]}> 1 Upper Case, </Text>
          <Text style={[IsContaindigit ? {color: Constant.OrangeColor} : {color: Constant.LightGrayColor}, loginstyles.validationtext]}> 1 Digit, </Text>
          <Text style={[IsContainSpecial ? {color: Constant.OrangeColor} : {color: Constant.LightGrayColor}, loginstyles.validationtext]}> 1 Special Character, </Text>
          <Text style={[IsContain8 ? {color: Constant.OrangeColor} : {color: Constant.LightGrayColor}, loginstyles.validationtext]}> 8 Characters, </Text> 
        </View>
    }
  }

  const handleBack = () => {
    if (navigation.canGoBack()) {
      navigation.goBack();
    } else {
      BackHandler.exitApp();
    }
    return true;
  };

  const headerback = () => {
    if (Platform.OS === 'ios') {
      return (<TouchableOpacity style={[loginstyles.Backcontainer, themeContainerStyle]} onPress={()=>{handleBack()}}>
          <Image source={require('../../assets/BackIC.png')} style={{'height': 30, 'width': 30}}/>
        </TouchableOpacity>
      );
    }
  }

  const Showloder = () => {
    return <View style={[ThemeStyle.loadingAnimation, themeContainerStyle]}>
        <Loadinganimation/>
    </View>;
  }

  return (
    IsLoader ?
    Showloder() : (
    <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "height"} style={{flex: 1}}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <SafeAreaView style={[loginstyles.maincontainer, themeContainerStyle]}>
          <StatusBar barstyles={themeStatusBarStyle}/>

          {headerback()}

          <View style={[loginstyles.container, themeContainerStyle]}>
            <Text style={loginstyles.titlefont}> Login </Text>
            <Text style={[loginstyles.subtitle, themeTextStyle]}> {IsEmail ? "Step 2: Enter Your Password" : "Step 1: Enter Your Email"} </Text>

            <View style={loginstyles.inputbox}>
              <TextInput style={themeTextStyle}
                underlineColorAndroid = "transparent"
                placeholder = {IsEmail ? "Password" : "E-mail"}
                placeholderTextColor = {Constant.LightGrayColor}
                autoCapitalize = "none"
                secureTextEntry = {IsEmail ? true : false}
                onChangeText = {(text) => IsEmail ? validatePassword(text) : validateEmail(text)} 
                value={IsEmail ? Password.toString() : Email.toString()}
              /> 
            </View>

            {displayPassMessage()}

            <TouchableOpacity style={loginstyles.loginBTN} onPress = {() => validate()} >
              <Text style={[{color: Constant.LightBlack, fontWeight: "bold"}, themeTextStyle]}>Next</Text>
            </TouchableOpacity>
          </View>

          <TouchableOpacity style={loginstyles.bottomText} >
            <Text style={themeTextStyle}><Text>Click Here To </Text><Text style={{fontWeight: "bold", fontStyle: 'normal', textDecorationLine: 'underline'}}>Registered</Text></Text>
          </TouchableOpacity>
        </SafeAreaView>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>)
  );
};

export default LoginScreen;