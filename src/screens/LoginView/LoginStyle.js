import { StyleSheet } from 'react-native';

import ThemeStyle from '../../utilities/ThemeStyle';
import Constant from '../../utilities/Constants';


const LoginStyle = StyleSheet.create({
    maincontainer: {
      flex: 1,
      width: '100%',
      height: '100%',
      backgroundColor: '#fff',
      // justifyContent: 'center',
      justifyContent: "space-around",
      alignContent: 'center',
    },
    Backcontainer: {
      alignItems: 'center',
      backgroundColor: '#fff',
      height: '7%',
      margin: '5%',
      width:'15%'
    },
    backBTN: {
      marginLeft: '0%',
      marginRight: '0%',
      marginTop: '0%',
    },
    container: { 
      flexGrow: 1,
      width: '100%',
      backgroundColor: '#fff',
      justifyContent: 'center',
      alignContent: 'center'
    },
    titlefont: {
      fontWeight: "bold", 
      color: Constant.OrangeColor,
      paddingLeft: '8%',
      fontSize: 50,
    },
    subtitle: {
      color: '#000',
      paddingLeft: '10%',
    },
    inputbox: {
      marginTop: '10%',
      padding: Platform.OS === 'ios' ? '3%' : 5,
      marginLeft: '10%',
      marginRight: '10%',
      maxWidth: '90%',
      maxHeight: Platform.OS === 'ios' ? '30%' : 50,
      borderWidth: 0.2,
      borderColor: '#D3D3D3'
    },
    validationbox: {
      marginTop: '5%',
      marginLeft: '10%',
      marginRight: '10%',
      flexDirection: 'column'
    },
    validationtext: {
      marginTop: '5%'
    },
    loginBTN: {
      marginTop: '10%',
      marginLeft: '10%',
      padding: '4%',
      paddingStart: '10%',
      paddingEnd: '10%',
      maxWidth: '40%',
      maxHeight: '25%',
      borderWidth: 0.2,
      alignItems: 'center',
      borderColor: '#D3D3D3'
    },
    bottomText: {
      flex: .2,
      justifyContent: 'center',
      paddingLeft: '10%',
    },
  });

  export default LoginStyle;