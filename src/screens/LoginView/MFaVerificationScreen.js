import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { View, Platform, useColorScheme, Appearance } from 'react-native';
import { AppearanceProvider } from 'react-native-appearance';
import { Font } from 'expo'
import Constants from '../../utilities/Constants';
import MFaVerificationStyle from './MFaVerificationStyle';
import ThemeStyle from '../../utilities/ThemeStyle';


export default function App ({navigation}) {

    const colorScheme = useColorScheme();
    const themeStatusBarStyle = colorScheme === 'light' ? 'dark-content' : 'light-content';
    const themeTextStyle = colorScheme === 'light' ? ThemeStyle.lightText : ThemeStyle.darkText;
    const themeContainerStyle = colorScheme === 'light' ? ThemeStyle.lightBG : ThemeStyle.darkBG;

    return (
    <View style={[MFaVerificationStyle.maincontainer, themeContainerStyle]}>
        <View style={[MFaVerificationStyle.container]} />
    </View>
    );
}