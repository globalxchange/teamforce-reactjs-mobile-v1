import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View, Image, Text, TouchableOpacity, Platform, useColorScheme } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import Constant from '../../utilities/Constants';
import welcomestyle from './WelcomeStyle';
import ThemeStyle from '../../utilities/ThemeStyle';

const WelcomeScreen = ({navigation}) => {

    const colorScheme = useColorScheme();
    const themeStatusBarStyle = colorScheme === 'light' ? 'dark-content' : 'light-content';
    const themeTextStyle = colorScheme === 'light' ? ThemeStyle.lightText : ThemeStyle.darkText;
    const themeContainerStyle = colorScheme === 'light' ? ThemeStyle.lightBG : ThemeStyle.darkBG;

    return (
        <SafeAreaProvider>
            <View style={[welcomestyle.maincontainer, themeContainerStyle]}>
            <StatusBar barstyles={themeStatusBarStyle}/>
                <View style={[welcomestyle.container, themeContainerStyle]}>
                    <Image source={Constant.logo} style={welcomestyle.logosetup}/>
                    <Image source={Constant.logonamelogoname} style={welcomestyle.namesetup}/>
                </View>
                <View style={[welcomestyle.optioncontainer, themeContainerStyle]}>
                    <TouchableOpacity style={welcomestyle.loginBTN} onPress={()=>{navigation.navigate('Login')}}>
                        <Text style={[{color: Constant.WhiteColor}]}>Login</Text>
                    </TouchableOpacity>
                    <Text style={{color: Constant.SepratorColor}}> ──────────────── </Text>
                    <TouchableOpacity style={welcomestyle.createBTN}>
                        <Text style={[{color: Constant.OrangeColor}, themeTextStyle]}>Create</Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity style={welcomestyle.footer}>
                        <Text style={[themeTextStyle, {justifyContent: 'center', alignItems: 'center'}]}><Text>Click Here If You Were</Text><Text style={{fontWeight: "bold"}}> Pre-Registered</Text></Text>
                </TouchableOpacity>
            </View>
        </SafeAreaProvider>
    );
};

export default WelcomeScreen;