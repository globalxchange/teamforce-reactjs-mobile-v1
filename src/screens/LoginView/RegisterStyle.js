import { StyleSheet, Platform, Dimensions } from 'react-native';
import ThemeStyle from '../../utilities/ThemeStyle';

export const RegisterStyle = StyleSheet.create({
    maincontainer: {
      flex: 1,
      width: '100%',
      height: '100%',
      backgroundColor: '#fff',
      justifyContent: 'center',
      alignContent: 'center',
      alignItems: 'center'
    },
    container: {
      width: '50%',
      height: '20%',
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center'
    },
  });

export default RegisterStyle;