import { StyleSheet, Platform, Dimensions } from 'react-native';
import ThemeStyle from '../../utilities/ThemeStyle';
import Constant from '../../utilities/Constants';

const {width} = Dimensions.get('window');

export const WelcomeStyle = StyleSheet.create({
    maincontainer: {
      flex: 1,
      width: '100%',
      height: '100%',
      backgroundColor: '#fff',
      justifyContent: 'center',
      alignContent: 'center',
      alignItems: 'center'
    },
    container: {
      width: '100%',
      height: '50%',
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center'
    },
    optioncontainer: {
      width: '100%',
      height: '20%',
      justifyContent: 'space-between',
      alignItems: 'center',
      flexDirection: 'column'
    },
    logosetup: {
      width: Platform.OS === 'ios' ? width * 0.45 : 150,
      height: Platform.OS === 'ios' ? width * 0.5 : 150,
      alignItems: 'center',
      justifyContent: 'center',
      resizeMode: 'stretch'
    },
    namesetup: {
      marginTop: '8%',
      marginBottom: '10%',
      width: '60%',
      height: '8%',
      alignItems: 'center',
      justifyContent: 'center',
      resizeMode: 'stretch'
    },
    loginBTN: {
      flex: 1,
      paddingStart: '25%',
      paddingEnd: '25%',
      maxWidth: '100%',
      maxHeight: '30%',
      backgroundColor: Constant.OrangeColor,
      justifyContent: 'center'
    },
    createBTN: {
      flex: 1,
      paddingStart: '25%',
      paddingEnd: '25%',
      maxWidth: '100%',
      maxHeight: '30%',
      justifyContent: 'center',
      borderWidth: 0.2,
      borderColor: Constant.OrangeColor
    },
    footer: {
      flex: .5, 
      width: '100%',
      justifyContent: 'center', 
      alignItems: 'center',
      alignContent: 'center',
      paddingLeft: '10%',
      marginTop: '20%'
    }
  });

export default WelcomeStyle;