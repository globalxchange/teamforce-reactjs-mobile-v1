import { StatusBar } from 'expo-status-bar';
import React, { Component, useState } from 'react';
import { 
    View,  
    Platform, 
    useColorScheme, 
    Text,
    TouchableOpacity,
    Image,
} from 'react-native';
import FromAppStyle from './FromAppStyle';
import ThemeStyle from '../../utilities/ThemeStyle';
import BottomSheetLayout from '../../layouts/BottomSheetLayout';
import Constant from '../../utilities/Constants';
import TMheaderView from '../../components/TMheaderView';
import TransactionHeader from '../../components/TransactionHeader';
import Loadinganimation from '../../utilities/LoadingAnimation';
import AppsPopupScreen from './AppsPopupScreen';

const FromAppScreen = ({navigation, isOpen, setIsOpen, ActionImage, TransferOption}) => {

    const colorScheme = useColorScheme();
    const themeStatusBarStyle = colorScheme === 'light' ? 'dark-content' : 'light-content';
    const themeTextStyle = colorScheme === 'light' ? ThemeStyle.lightText : ThemeStyle.darkText;
    const themeContainerStyle = colorScheme === 'light' ? ThemeStyle.lightBG : ThemeStyle.darkBG;
    const [isLoader, setIsLoader] = useState(false)
    const [isSelectApp, setisSelectApp] = useState(false)


    const Showloder = () => {
        return <View style={[ThemeStyle.loadingAnimation, themeContainerStyle]}>
            <Loadinganimation/>
        </View>;
    }

    const getSelected = () => {
        return (
            <>
                <Image style={{width: 25, height: 25, marginRight: 20, margin: 20}} source={Constant.AnotherGXIC} />
                <Text style={[FromAppStyle.optiontitle, themeTextStyle]}> To Another GX User </Text>
            </>
        );
    }

    const getunselected = () => {
        return (
            <>
                <Text style={[FromAppStyle.optiontitle, themeTextStyle]}> See All Registered Apps </Text>
                <Image style={{width: 25, height: 10, resizeMode:'center'}} source={Constant.BottomArrorIC} />
            </>
        )
    }

    const viewcontainer = () => {
        return (
            <View style={[FromAppStyle.maincontainer, themeContainerStyle]} >
                <TMheaderView BGcolor={{backgroundColor: Constant.OrangeColor}} IsSingle={true} SingleImage={Constant.ConnectWhiteIC}/>
                <TransactionHeader IsSend={true} IsToCurrency={false} IsReciepent={false} IsAmount={false}/>
                <Text style={[FromAppStyle.headtitle, themeTextStyle]}>Select App From</Text>
                <Text style={[FromAppStyle.subtitle, themeTextStyle]}>Which One Of Your GX Apps Are You Going To Send The Funds From</Text>
                <TouchableOpacity style={[FromAppStyle.optioncell, {shadowColor: colorScheme === 'light' ? Constant.LightBlack : Constant.AliceBlue, borderColor: colorScheme === 'light' ? Constant.LightBlack : Constant.AliceBlue}]} onPress={() => {setisSelectApp(true)}}>
                    { //isSelectApp ? 
                    // getSelected() : 
                    getunselected()
                    }
                </TouchableOpacity>
                <TouchableOpacity style={[Constant.CommonStylesheet.centerButton]} >
                    <Text style={{fontSize: 20, color: '#fff', }}> Proceed To Select Currency </Text>
                </TouchableOpacity>
            </View>
        );
    }

    return (
        <>
            <BottomSheetLayout 
            isOpen={isOpen} 
            onClose={() => setIsOpen(false)}
            reactToKeyboard 
            height={Constant.ScreenWidth * 1.3} >
                { isLoader ? Showloder() : viewcontainer()}
            </BottomSheetLayout>
            <AppsPopupScreen isOpen={isSelectApp} setIsOpen={setisSelectApp}/>
        </>
    );
};

export default FromAppScreen;