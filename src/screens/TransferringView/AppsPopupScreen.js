import { StatusBar } from 'expo-status-bar';
import React, {useState } from 'react';
import { 
    View,  
    Platform, 
    useColorScheme, 
    Text,
    TouchableOpacity,
    Image,
} from 'react-native';
import FromAppStyle from './FromAppStyle';
import ThemeStyle from '../../utilities/ThemeStyle';
import PopupLayout from '../../layouts/PopupLayout';
import Constant from '../../utilities/Constants';
import TMheaderView from '../../components/TMheaderView';
import TransactionHeader from '../../components/TransactionHeader';
import Loadinganimation from '../../utilities/LoadingAnimation';
// import SearchView from '../../layouts/SearchLayout';

const AppsPopupScreen = ({isOpen, setIsOpen, TransferOption}) => {

    const colorScheme = useColorScheme();
    const themeStatusBarStyle = colorScheme === 'light' ? 'dark-content' : 'light-content';
    const themeTextStyle = colorScheme === 'light' ? ThemeStyle.lightText : ThemeStyle.darkText;
    const themeContainerStyle = colorScheme === 'light' ? ThemeStyle.lightBG : ThemeStyle.darkBG;
    const [isLoader, setIsLoader] = useState(false)


    const Showloder = () => {
        return <View style={[ThemeStyle.loadingAnimation, themeContainerStyle]}>
            <Loadinganimation/>
        </View>;
    }

    const viewcontainer = () => {
        return (
            <View style={[FromAppStyle.maincontainer, themeContainerStyle]} >
                <TMheaderView BGcolor={{backgroundColor: Constant.OrangeColor}} IsSingle={true} SingleImage={Constant.ConnectWhiteIC}/>
                <TransactionHeader IsSend={true} IsToCurrency={false} IsReciepent={false} IsAmount={false}/>
                <Text style={[FromAppStyle.headtitle, themeTextStyle]}>Select App From</Text>
            </View>
        );
    }

    return (
        <>
            <PopupLayout 
            isOpen={isOpen} 
            onClose={() => setIsOpen(false)}
            reactToKeyboard 
            height={Constant.ScreenWidth * 1.2} 
            headerTitle={'All Your Apps'} >
                {/* <SearchView placeholder={'Search Your Apps With Balances'}>

                </SearchView> */}
            </PopupLayout>
        </>
    );
};

export default AppsPopupScreen;