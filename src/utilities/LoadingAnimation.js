import React from 'react';
import {StyleSheet, View, useColorScheme, Appearance} from 'react-native';
import LottieView from 'lottie-react-native';
import { AppearanceProvider } from 'react-native-appearance';

const LoadingAnimation = ({height, width, Darkmode}) => {
  const colorScheme = useColorScheme();
  Darkmode = colorScheme === 'light' ? false : true 
  return (
    <View style={styles.container}>
      <LottieView
        style={{width: width || '80%', height: height || '80%'}}
        source={Darkmode ? require('../assets/Loading_dark.json') : require('../assets/Loading_light.json')}
        autoPlay
        loop
      />
    </View>
  );
};

export default LoadingAnimation;

const styles = StyleSheet.create({
  container: {
    flex:1,
    flexDirection:'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    alignContent: 'space-around'
  },
  subcontainer: {
    width: '50%',
    height: '20%',
    alignItems: 'center',
    justifyContent: 'center'
  },
});
