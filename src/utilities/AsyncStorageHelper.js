// import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';

const setLoginSession = async (data) => {
  try {
    if (data !== null) {
      await AsyncStorage.setItem('LoginSession', JSON.stringify(data));
      console.log('Data stored in async storage:-', data);
      await setIsLoggedIn(true)
      await setLoginEmail(data.user.email)
      await setidToken(data.idToken)
      await setAccessToken(data.accessToken)
      await setRefreshToken(data.refreshToken)
      await setDeviceKey(data.device_key)
    }
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
};

const getLoginession = async () => {
  let LoginSession;
  try {
    LoginSession = (await AsyncStorage.getItem('LoginSession'));
  } catch (error) {
    console.log('Error on setting AsyncStorage data:-', error);
  }
  return LoginSession;
};

const removeLoginSession = async (bool) => {
  try {
    await AsyncStorage.removeItem('LoginSession');
    await AsyncStorage.removeItem('isLoggedIn');
    await AsyncStorage.removeItem('email');
    await AsyncStorage.removeItem('idToken');
    await AsyncStorage.removeItem('accessToken');
    await AsyncStorage.removeItem('refreshToken');
    await AsyncStorage.removeItem('device_key');
  } catch (error) {
    // Error retrieving data
    console.log(error.message);
    return false;
  }
  return true;
};

const setIsLoggedIn = async (bool) => {
  try {
    await AsyncStorage.setItem('isLoggedIn', JSON.stringify(bool));
  } catch (error) {
    console.log('Error on setting AsyncStorage data:-', error);
  }
};

const getIsLoggedIn = async () => {
  let isLoggedIn;
  try {
    isLoggedIn = (await AsyncStorage.getItem('isLoggedIn')) || false;
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
  return isLoggedIn;
};

const setLoginEmail = async (email) => {
  try {
    await AsyncStorage.setItem('email', email);
    console.log('Email stored in async storage:-', email);
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
};

const getLoginEmail = async () => {
  let email;
  try {
    email = (await AsyncStorage.getItem('email')) || '';
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
  return email;
};

const setidToken = async (token) => {
  try {
    await AsyncStorage.setItem('idToken', token);
    await setIsLoggedIn(true);
    console.log('Token stored in async storage:-', token);
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
};

const getidToken = async () => {
  let appToken;
  try {
    appToken = (await AsyncStorage.getItem('idToken')) || '';
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
  return appToken;
};

const setAccessToken = async (accessToken) => {
  try {
    await AsyncStorage.setItem('accessToken', accessToken);
    await setIsLoggedIn(true);
    console.log('AccessToken stored in async storage:-', accessToken);
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
};

const getAccessToken = async () => {
  let accessToken;
  try {
    accessToken = (await AsyncStorage.getItem('accessToken')) || '';
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
  return accessToken;
};

const setRefreshToken = async (refreshToken) => {
  try {
    await AsyncStorage.setItem('refreshToken', refreshToken);
    await setIsLoggedIn(true);
    console.log('RefreshToken stored in async storage:-', refreshToken);
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
};

const getRefreshToken = async () => {
  let refreshToken;
  try {
    refreshToken = (await AsyncStorage.getItem('refreshToken')) || '';
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
  return refreshToken;
};

const setDeviceKey = async (deviceKey) => {
  try {
    await AsyncStorage.setItem('device_key', deviceKey);
    await setIsLoggedIn(true);
    console.log('DeviceKey stored in async storage:-', deviceKey);
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
};

const getDeviceKey = async () => {
  let deviceKey;
  try {
    deviceKey = (await AsyncStorage.getItem('device_key')) || '';
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
  return deviceKey;
};

const AsyncStorageHelper = {
  setLoginSession,
  getLoginession,
  removeLoginSession,
  setIsLoggedIn,
  getIsLoggedIn,
  setLoginEmail,
  getLoginEmail,
  setidToken,
  getidToken,
  setAccessToken,
  getAccessToken,
  setRefreshToken,
  getRefreshToken,
  setDeviceKey,
  getDeviceKey,
};

export default AsyncStorageHelper;
