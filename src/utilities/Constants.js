import { Dimensions, StyleSheet } from 'react-native';

export const ScreenWidth = Dimensions.get('window').width;
export const ScreenHeight = Dimensions.get('window').height;
export const ScreenDimension = Dimensions.get('window');

export const OrangeColor = '#F45D48';
export const LightGrayColor = '#616161';
export const SepratorColor = '#D3D3D3';
export const WhiteColor = '#fff';
export const BlackColor = '#000';
export const BorderLightColor = '#bdbdbd';
export const BorderDarkColor = '#575c66';
export const Light_textColor = '#171717';
export const Dark_textColor = '#ECEFF4';
export const DarkText = '#555770';
export const DarkBGColor = '#242C40';
export const BottomBarColor = '#EBEBEB';
export const AliceBlue = '#F0F8FF';
export const Azure = '#F0FFFF';
export const WhiteSmoke = '#F5F5F5';
export const LightBlack = '#C0C0C0';
export const APP_MAIN_COLOR = '#08152D';
export const Light_White_Border = '#EBEBEB';

export const logo = require("../assets/Team-LOGO.png");
export const logoname = require("../assets/Team-NAME.png");
export const RearIC = require("../assets/GameTimeIC.png");
export const FitnessIC = require("../assets/FitnessIC.png");
export const RedHeartIC = require("../assets/RedHeartIC.png");
export const TimerIC = require("../assets/TimerIC.png");
export const ProfileIC = require("../assets/ProfileIC.png");
export const NavLogo = require("../assets/Nav-LOGO.png");
export const BlockckcheckIcon = require("../assets/Plugin/BlockckcheckIcon.png");
export const ChatsIOIcon = require("../assets/Plugin/Chats-ioIcon.png");
export const MoneyMarketsIcon = require("../assets/Plugin/MoneyMarketsIcon.png");
export const SupportIcon = require("../assets/Plugin/SupportIcon.png");
export const RefreshIcon = require("../assets/Plugin/RefreshIcon.png");
export const ConnectIcon = require("../assets/Plugin/ConnectIcon.png");
export const WalletsIC = require("../assets/Plugin/Transfer/WalletsIC.png");
export const AnotherGXIC = require("../assets/Plugin/Transfer/AnotherGXIC.png");
export const ArroesIC = require("../assets/Plugin/Transfer/ArroesIC.png");
export const BankIC = require("../assets/Plugin/Transfer/BankIC.png");
export const ConnectWhiteIC = require("../assets/Plugin/Transfer/ConnectWhiteIC.png");
export const CurrencyIC = require("../assets/Plugin/Transfer/CurrencyIC.png");
export const EcommerceIC = require("../assets/Plugin/Transfer/EcommerceIC.png");
export const RecepientIC = require("../assets/Plugin/Transfer/RecepientIC.png");
export const AppUpdateIC = require("../assets/AppUpdateIC.png");
export const SendIC = require("../assets/SendIC.png");
export const MenuIC = require("../assets/MenuIC.png");
export const BottomArrorIC = require("../assets/BottomArrorIC.png");

export const loginURL = "https://gxauth.apimachine.com/gx/user/login";
export const AppUpdateURL = "https://comms.globalxchange.com/gxb/apps/mobile/app/links/logs/get?app_code=teamforce";
export const GX_AUTH_URL = 'https://gxauth.apimachine.com/gx/user/refresh/tokens';
export const GX_API_ENDPOINT = 'https://comms.globalxchange.com/brokerage/verify_token';
export const Get_AllApp_URL = "https://comms.globalxchange.com/gxb/apps/registered/user?";
export const COGNITO_USER_POOL_ID = 'us-east-2_ALARK1RAa';

export const FONT_BOLD = 'Montserrat-Bold';
export const FONT_EXTRA_BOLD = 'Montserrat-ExtraBold';
export const FONT_EXTRA_LIGHT = 'Montserrat-ExtraLight';
export const FONT_LIGHT = 'Montserrat-Light';
export const FONT_NORMAL = 'Montserrat-Regular';
export const FONT_MEDIUM = 'Montserrat-Medium';
export const FONT_SEMI_BOLD = 'Montserrat-SemiBold';

export const DeviceInfo = require('react-native-device-info');


export const CommonStylesheet = StyleSheet.create({
  centerButton: {
    flex: 1,
    height: '50%',
    marginTop: '10%',
    marginBottom: '5%',
    borderRadius: 15,
    backgroundColor: OrangeColor,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: '10%',
    paddingRight: '10%',
  },
  bottomfullButton: {
    color: OrangeColor,
    fontSize: 15,
    marginLeft: '5%',
    padding: '2%',
    borderWidth: 0.2,
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  }
});

export default {
  ScreenWidth,
  ScreenHeight,
  ScreenDimension,
  OrangeColor,
  LightGrayColor,
  SepratorColor,
  WhiteColor,
  BlackColor,
  BorderLightColor,
  BorderDarkColor,
  Light_textColor,
  Dark_textColor,
  DarkText,
  DarkBGColor,
  BottomBarColor,
  AliceBlue,
  Azure,
  WhiteSmoke,
  LightBlack,
  APP_MAIN_COLOR,
  Light_White_Border,
  logo,
  logoname,
  RearIC,
  FitnessIC,
  RedHeartIC,
  TimerIC,
  ProfileIC,
  SendIC,
  MenuIC,
  NavLogo,
  BlockckcheckIcon,
  ChatsIOIcon,
  MoneyMarketsIcon,
  SupportIcon,
  RefreshIcon,
  ConnectIcon,
  WalletsIC,
  AnotherGXIC,
  ArroesIC,
  BankIC,
  ConnectWhiteIC,
  CurrencyIC,
  EcommerceIC,
  RecepientIC,
  loginURL,
  AppUpdateIC,
  BottomArrorIC,
  AppUpdateURL,
  GX_AUTH_URL,
  Get_AllApp_URL,
  GX_API_ENDPOINT,
  COGNITO_USER_POOL_ID,
  FONT_BOLD,
  FONT_EXTRA_BOLD,
  FONT_EXTRA_LIGHT,
  FONT_LIGHT,
  FONT_NORMAL,
  FONT_MEDIUM,
  FONT_SEMI_BOLD,
  DeviceInfo,
  CommonStylesheet,
};