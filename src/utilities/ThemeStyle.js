import { StyleSheet, Platform, Dimensions } from 'react-native';
import Constants from './Constants';

export const ThemeStyle = StyleSheet.create({
    lightBG: {
      backgroundColor: Constants.WhiteColor
    },
    lightBorder: {
      borderColor: Constants.BorderLightColor
    },
    lightText: {
      color: Constants.Light_textColor
    },
    darkBG: {
      backgroundColor: Constants.DarkBGColor
    },
    darkBorder: {
      borderColor: Constants.BorderDarkColor
    },
    darkText: {
      color: Constants.Dark_textColor
    },
    loadingAnimation: {
      width: '100%',
      height: '100%',
      justifyContent: 'center',
      alignItems: 'center'
    },
  });

  export default ThemeStyle;