
/* eslint-disable react-native/no-inline-styles */
import {Image, StyleSheet, Text, TouchableOpacity, View, useColorScheme, Dimensions} from 'react-native';
import Constant from '../utilities/Constants';
import AsyncStorageHelper from '../utilities/AsyncStorageHelper';
import ThemeStyle from '../utilities/ThemeStyle';
import React, {useState, useEffect} from 'react';

const {width} = Dimensions.get('window');

const ProfileAvatar = ({navigation}) => {

  const colorScheme = useColorScheme();
  const themeStatusBarStyle = colorScheme === 'light' ? 'dark-content' : 'light-content';
  const themeTextStyle = colorScheme === 'light' ? ThemeStyle.lightText : ThemeStyle.darkText;
  const themeContainerStyle = colorScheme === 'light' ? ThemeStyle.lightBG : ThemeStyle.darkBG;

  const [isLoggedIn, setisLoggedIn] = useState(false);
  const [isemail, setisemail] = useState("");
  const [loginsession, setloginsession] = useState(null);
  
  useState( async () => {
    setisLoggedIn(await AsyncStorageHelper.getIsLoggedIn());
  });

  useState( async () => {
    setisemail(await AsyncStorageHelper.getLoginEmail());
  });

  useState( async () => {
    setloginsession(await AsyncStorageHelper.getLoginession());
  });
  
  return (
      <View style={[styles.maincontainer, themeContainerStyle]}>
        <TouchableOpacity style={[styles.container, {'borderColor': colorScheme === 'light' ? Constant.LightBlack : Constant.AliceBlue }]} >
          <Image style={styles.proimg} source={Constant.ProfileIC} resizeMode="contain"/>
        </TouchableOpacity>
        <Text style={[styles.titlestyle, themeTextStyle]} >{isemail}</Text>
        <Text style={[styles.subtitlestyle, themeTextStyle]} >{isemail}</Text>
      </View>
  );
};

export default ProfileAvatar;

const styles = StyleSheet.create({
  maincontainer: {
    width: '100%',
    height: width * 0.60,
    backgroundColor: '#000',
    alignItems: 'center',
    padding: 10,
  },  
  container: {
    flex: 1,
    width: width * 0.35,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
    padding: 10,
    borderRadius: 100,
  },
  proimg: {
    width: '100%',
    height: '100%',
    borderRadius: 100,
  },
  titlestyle: {
    color: '#555770',
    fontSize: 35,
    marginTop: 15
  },
  subtitlestyle: {
    color: '#555770',
    marginTop: 5,
  }
});
