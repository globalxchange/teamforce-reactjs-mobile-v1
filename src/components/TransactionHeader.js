import React from 'react';
import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  Platform, 
  useColorScheme, 
  Text
} from 'react-native';
import Constant from '../utilities/Constants';
import ThemeStyle from '../utilities/ThemeStyle';


const {width} = Dimensions.get('window');


const TransactionHeader = ({IsSend, IsToCurrency, IsReciepent, IsAmount}) => {

  const colorScheme = useColorScheme();
  const themeStatusBarStyle = colorScheme === 'light' ? 'dark-content' : 'light-content';
  const themeTextStyle = colorScheme === 'light' ? ThemeStyle.lightText : ThemeStyle.darkText;
  const themeContainerStyle = colorScheme === 'light' ? ThemeStyle.lightBG : ThemeStyle.darkBG;

  return (
    <View style={[styles.maincontainer, themeContainerStyle]}>
        <View style={[styles.action, {opacity: IsSend ? 1.0 : 0.3}]}>
            <Image style={styles.appIcon} resizeMode="contain" source={Constant.CurrencyIC} />
            <Text style={[styles.apptitle, themeTextStyle]} >Send Currency</Text>
        </View> 
        <Image style={{height: 20, width: 20,}} resizeMode="contain" source={Constant.ArroesIC} />
        <View style={[styles.action, {opacity: IsToCurrency ? 1.0 : 0.2}]}>
            <Image style={styles.appIcon} resizeMode="contain" source={Constant.BankIC} />
            <Text style={[styles.apptitle, themeTextStyle]} >To Currency</Text>
        </View> 
        <Image style={{height: 20, width: 20,}} resizeMode="contain" source={Constant.ArroesIC} />
        <View style={[styles.action, {opacity: IsReciepent ? 1.0 : 0.2}]}>
            <Image style={styles.appIcon} resizeMode="contain" source={Constant.RecepientIC} />
            <Text style={[styles.apptitle, themeTextStyle]} >Recepient</Text>
        </View> 
        <Image style={{height: 20, width: 20,}} resizeMode="contain" source={Constant.ArroesIC} />
        <View style={[styles.action, {opacity: IsAmount ? 1.0 : 0.2}]}>
            <Image style={styles.appIcon} resizeMode="contain" source={Constant.EcommerceIC} />
            <Text style={[styles.apptitle, themeTextStyle]} >Amount</Text>
        </View> 
    </View>
  );
};

export default TransactionHeader;

const styles = StyleSheet.create({
  maincontainer: {
    width: '100%',
    height: '12%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 10,
    paddingLeft: 20,
    paddingRight: 20,
  },
  action: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  appIcon: {
    height: 30,
    width: 30,
  },
  apptitle: {
    fontSize: 10,
    marginTop: 10
  },
});

