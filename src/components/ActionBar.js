import React, {useState, useContext} from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Keyboard,
  Dimensions,
  Platform, 
  useColorScheme, 
  Appearance,
  SafeAreaView,
} from 'react-native';
import SafeAreaProvider from 'react-native-safe-area-context';
import {useNavigation} from '@react-navigation/native';
import Constant from '../utilities/Constants';
import ThemeStyle from '../utilities/ThemeStyle';
import { divide } from 'react-native-reanimated';
import PluginScreen from '../screens/PluginScreen/PluginScreen';
import TransferScreen from '../screens/PluginScreen/TransferScreens/TransferingScreen';

const {width} = Dimensions.get('window');


const ActionBar = ({navigation}) => {

  const navigations = useNavigation();

  const colorScheme = useColorScheme();
  const themeStatusBarStyle = colorScheme === 'light' ? 'dark-content' : 'light-content';
  const themeTextStyle = colorScheme === 'light' ? ThemeStyle.lightText : ThemeStyle.darkText;
  const themeContainerStyle = colorScheme === 'light' ? ThemeStyle.lightBG : ThemeStyle.darkBG;
  const [isOpen, setIsOpen] = useState(false);

  return (
    <>
    <SafeAreaView>
      <View style={[ styles.container, themeContainerStyle, {'borderBottomColor': colorScheme === 'light' ? Constant.LightBlack : Constant.AliceBlue }]}>
        <TouchableOpacity style={[styles.toggleButton, themeContainerStyle]} onPress={() => {navigation.navigate('DrawerMenuScreen')}}>
          <Image source={require('../assets/MenuIC.png')} style={{'height': 20, 'width': 20}}/>
        </TouchableOpacity>
        <View style={[styles.appIconContainer, themeContainerStyle]}>
          <Image style={styles.appIcon} resizeMode="contain" source={Constant.NavLogo} />
        </View>
        <TouchableOpacity style={[styles.sendButton, themeContainerStyle]} onPress={() => {setIsOpen(true)}}>
          <Image source={require('../assets/SendIC.png')} style={{'height': 20, 'width': 20}}/>
        </TouchableOpacity>
      </View>
      <PluginScreen navigation={navigation} isOpen={isOpen} setIsOpen={setIsOpen}/>
      {/* <TransferScreen navigation={navigation} isOpen={isOpen} setIsOpen={setIsOpen}/> */}
    </SafeAreaView>
    </>
  );
};

export default ActionBar;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 70,
    display: 'flex',
    flexDirection: 'row',
    position: 'relative',
    borderBottomWidth: 1,
  },
  toggleButton: {
    marginTop: 'auto',
    marginBottom: 'auto',
    marginLeft: '5%',
    height: '100%',
    width: '15%',
    justifyContent: 'center',
  },
  sendButton: {
    marginTop: 'auto',
    marginBottom: 'auto',
    height: '100%',
    justifyContent: 'center',
    marginLeft: 'auto',
    width: '14%',
  },
  appIconContainer: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: -1,
  },
  appIcon: {
    height: 36,
    width: width / 2.6,
    marginTop: 'auto',
    marginBottom: 'auto',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
});
