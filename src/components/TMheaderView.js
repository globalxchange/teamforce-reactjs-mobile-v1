import React from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Keyboard,
  Dimensions,
  Platform, 
  useColorScheme, 
  Appearance,
  SafeAreaView,
} from 'react-native';
import Constant from '../utilities/Constants';
import ThemeStyle from '../utilities/ThemeStyle';


const {width} = Dimensions.get('window');


const TMheaderView = ({BGcolor, IsSingle, SingleImage}) => {

  const colorScheme = useColorScheme();
  const themeStatusBarStyle = colorScheme === 'light' ? 'dark-content' : 'light-content';
  const themeTextStyle = colorScheme === 'light' ? ThemeStyle.lightText : ThemeStyle.darkText;
  const themeContainerStyle = colorScheme === 'light' ? ThemeStyle.lightBG : ThemeStyle.darkBG;

  const doubleheader = () => {
    return (
      <View style={[styles.maincontainer, BGcolor, {'borderBottomColor': colorScheme === 'light' ? Constant.LightBlack : Constant.AliceBlue }]}>
        <Image style={styles.appIcon} resizeMode="contain" source={Constant.logo} />
        <Image style={styles.applogo} resizeMode="contain" source={Constant.logoname} />
      </View>
    );
  }

  const singleheader = () => {
    return (
      <View style={[styles.maincontainer, BGcolor, {'borderBottomColor': colorScheme === 'light' ? Constant.LightBlack : Constant.AliceBlue }]}>
        <Image style={styles.applogo} resizeMode="contain" source={SingleImage} />
      </View>
    );
  }

  return (
    <>
    { IsSingle ? singleheader() : doubleheader() }
    </>
  );
};

export default TMheaderView;

const styles = StyleSheet.create({
    maincontainer: {
    width: '100%',
    height: '13%',
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    flexDirection: 'row',
    position: 'relative',
    borderBottomWidth: 1,
  },
  appIcon: {
    height: 40,
    width: 40,
    marginRight: 15,
  },
  applogo: {
    height: 40,
    width: width / 2.6,
  },
});
